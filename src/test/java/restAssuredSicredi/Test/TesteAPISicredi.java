package restAssuredSicredi.Test;

import org.apache.http.HttpStatus;
import org.hamcrest.Matchers;
import org.junit.Test;
import restAssuredSicredi.Domain.UserCep;

import static io.restassured.RestAssured.given;
import static io.restassured.http.ContentType.HTML;
import static io.restassured.http.ContentType.JSON;
import static org.hamcrest.CoreMatchers.containsString;
import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.MatcherAssert.assertThat;

public class TesteAPISicredi extends BaseTest {

    private static final String MOSTRAR_CEP_CONSULTADO = "/91060900/json";
    private static final String MOSTRAR_CEP_INEXISTENTE = "/99999999/json";
    private static final String MOSTRAR_CEP_INVALIDO = "/950100100/json";
    private static final String MOSTRAR_ENDERECO = "/RS/Gravatai/Barroso/json/";

    @Test
    public void testCepValido() {
        UserCep userCep = given().
                log().all().
                get(MOSTRAR_CEP_CONSULTADO).
                then().
                contentType(JSON).
                log().body().
                statusCode(HttpStatus.SC_OK).
                extract().
                body().jsonPath().getObject("", UserCep.class);

        assertThat(userCep.getCEP(), containsString("91060-900"));
        assertThat(userCep.getLogradouro(), containsString("Avenida Assis Brasil 3940"));
        assertThat(userCep.getComplemento(), containsString(""));
        assertThat(userCep.getBairro(), containsString("São Sebastião"));
        assertThat(userCep.getLocalidade(), containsString("Porto Alegre"));
        assertThat(userCep.getUf(), containsString("RS"));
        assertThat(userCep.getIbge(), containsString("4314902"));
    }

    @Test
    public void testConsultaCepInexistente() {
        given().
                log().all().
                get(MOSTRAR_CEP_INEXISTENTE).
                then().
                contentType(JSON).
                log().body().
                statusCode(HttpStatus.SC_OK).
                body("erro", is(true));
    }

    @Test
    public void testConsultaCepFormatoInvalido() {
        given().
                log().all().
                get(MOSTRAR_CEP_INVALIDO).
                then().
                contentType(HTML).
                log().body().
                statusCode(HttpStatus.SC_BAD_REQUEST);
    }

    @Test
    public void testConsultarEndereço() {
        given().
                log().all().
                when().
                get(MOSTRAR_ENDERECO).
                then().
                contentType(JSON).
                log().body().
                statusCode(HttpStatus.SC_OK).
                body("cep", Matchers.hasItems("94085-170", "94175-000")).
                body("logradouro", Matchers.hasItems("Rua Ari Barroso", "Rua Almirante Barroso")).
                body("complemento", Matchers.hasItems("", "")).
                body("bairro", Matchers.hasItems("Morada do Vale I", "Recanto Corcunda")).
                body("localidade", Matchers.hasItems("Gravataí", "Gravataí")).
                body("uf", Matchers.hasItems("RS", "RS")).
                body("ibge", Matchers.hasItems("4309209", "4309209"));

    }

}